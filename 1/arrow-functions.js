/**
 * Стрелочные функции.
 */

// Параметры по умолчанию

let defaultWidth = 100;

// Параметры по умолчанию могут быть не только значениями, но и выражениями.
function showMenu(title = "Без заголовка", width = defaultWidth, height = defaultWidth * 2) {
    alert(title + ' ' + width + ' ' + height);
}

showMenu("Меню"); // Меню 100 200

// Spread операторы можно использовать в функциях но оператор должен быть последним аргументом

function showName(firstName, lastName, ...rest) {
    alert(`${firstName} ${lastName} - ${rest}`);
}

showName("Юлий", "Цезарь", "Император", "Рима"); // Юлий Цезарь - Император,Рима

// Короткая форма записи функций

//let inc = function(x) { return x + 1; };
let inc = x => x+1;

console.log(inc(1)); // 2

// У стрелочных функций нет своей области видимости для this

let group = {
    title: "Наш курс",
    students: ["Вася", "Петя", "Даша"],

    showList: function() {
        this.students.forEach(
            student => alert(this.title + ': ' + student)
        )
    }
};

group.showList();
// Наш курс: Вася
// Наш курс: Петя
// Наш курс: Даша


let group_arrow = {
    title: "Наш курс",
    students: ["Вася", "Петя", "Даша"],

    showList: function() {
        this.students.forEach(function(student) {
            alert(this.title + ': ' + student); // будет ошибка
        })
    }
};

group.showList();

// У стрелочных функций нет доступа к объекту arguments

function withArguments() {
    console.log(arguments) // массив из строки test
}

withArguments('test');

let withoutArguments = () => console.log(arguments);
withoutArguments();  // ошибка
