/**
 * Данные. Структуры данных.
 */

// Шесть типов данных, typeof
// Есть 5 «примитивных» типов: number, string, boolean, null, undefined и 6-й тип – объекты object.

// Число «number»

var n = 123;
n = 12.345;
console.log(1 / 0); // Infinity
console.log("нечисло" * 2); // NaN, ошибка

// Строка «string»

var str = "Строка";

// Логический тип «boolean»

console.log(true || false);

// Специальное значение «undefined»

var x;
alert( x ); // выведет "undefined"

// Объекты «object»

var user = { name: "Вася" };
